from random import randint

name = input("Hi! What is your name? ")

for i in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)

    print("Guess", i, ":", name,
          "were you born in", month, "/", year)

    yesOrNo = input("Yes or No: ")
    if yesOrNo == "Yes" or yesOrNo == "No":
        if yesOrNo == "Yes":
            print("I knew it!")
            break
        elif i <= 4:
            print("Drat! Lemme try again!")
        else:
            print("I have other things to do. Good bye.")
    else:
        yesOrNo = input("You may only enter Yes or No. Yes or No: ")
